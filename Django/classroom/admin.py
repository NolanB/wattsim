from django.contrib import admin

# Register your models here.

# from classroom.models import Schoolroom, User, Album, Song, Student, Teacher
from classroom.models import Schoolroom, User, Student, Teacher, Subject
from classroom.models import Station, Measurement

# from classroom.models import Album, Song

@admin.register(Schoolroom)
class SchoolroomAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    fields = ('id', 'name')
    readonly_fields = ('id', )
    # list_display = ('Schoolroom', 'User')
    pass

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'station')
    # fields = ('id', 'name', 'teacher')
    readonly_fields = ('id', 'station')

admin.site.register(Teacher)
admin.site.register(Student)
admin.site.register(Subject)


@admin.register(Station)
class StationAdmin(admin.ModelAdmin):
    list_display = ('id',)
    fields = ('id','user')
    readonly_fields = ('id', )


@admin.register(Measurement)
class MeasurementAdmin(admin.ModelAdmin):
    list_display = ('id', 'station', 'recorded')
    fields = ('id', 'station', 'recorded', 'solar', 'wind', 'hydro')
    readonly_fields = ('id', 'recorded')
