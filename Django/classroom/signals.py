import requests
session = requests.Session()

# Authentification
from getpass import getpass
username = input("Nom d'utilisateur : ")
password = getpass('Mot de passe :')
response = session.post(
    'http://localhost:8000/api/v1/user/',
    json={'username': username, 'password': password},
)
response.raise_for_status()

# Paramétrage de la session pour renvoyer correctement les informations
# d'authentification
session.headers['Referer'] = 'http://localhost:8000'
session.headers['X-CSRFToken'] = session.cookies['csrftoken']

# Envoyer une mesure de sa station
response = session.post(
    'http://localhost:8000/api/v1/measurements/',
    json={
        'solar': 10.0,
        'wind': None,  # Pas de mesure à fournir
        'hydro': 2.0,
    }
)
response.raise_for_status()
print(response.json())