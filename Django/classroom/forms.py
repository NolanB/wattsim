from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from django.forms.utils import ValidationError
from classroom import WattsimDB as WDB
# from django.request import request

from classroom.models import (Answer, Question, Student, StudentAnswer,
                              Subject, User, Teacher, Schoolroom)

class TeacherSignUpForm(UserCreationForm):
    schoolroom = forms.ModelChoiceField(
        queryset=Schoolroom.objects.all(),
        widget=forms.RadioSelect,
        required=True,
        initial=0
    )
    print("schoolroom : ", schoolroom)
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username', 'first_name', 'last_name', 'schoolroom')

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_teacher = True
        cleaned_data = self.cleaned_data
        school_name = cleaned_data.get('schoolroom')
        schoolroom = Schoolroom.objects.filter(name=school_name)
        conn = WDB.connexion_DB()
        if conn is not None:
            schoolroom_id = WDB.select_classroom_id(conn, schoolroom[0])
            WDB.deconnexion_DB(conn)
            user.save()
            teacher = Teacher.objects.create(user=user, schoolroom=schoolroom[0])
            teacher.save()
            return user

class StudentSignUpForm(UserCreationForm):
    schoolroom = forms.ModelChoiceField(
        queryset=Schoolroom.objects.all(),
        widget=forms.RadioSelect,
        required=True,
        initial=0
    )
    interests = forms.ModelMultipleChoiceField(
        queryset=Subject.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username', 'first_name', 'last_name', 'schoolroom')

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_student = True
        cleaned_data = self.cleaned_data
        school_name = cleaned_data.get('schoolroom')
        schoolroom = Schoolroom.objects.filter(name=school_name)
        user.save()
        student = Student.objects.create(user=user, schoolroom=schoolroom[0])
        student.interests.add(*self.cleaned_data.get('interests'))
        student.save()
        return user


class StudentInterestsForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ('interests', )
        widgets = {
            'interests': forms.CheckboxSelectMultiple
        }


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ('text', )


class BaseAnswerInlineFormSet(forms.BaseInlineFormSet):
    def clean(self):
        super().clean()

        has_one_correct_answer = False
        for form in self.forms:
            if not form.cleaned_data.get('DELETE', False):
                if form.cleaned_data.get('is_correct', False):
                    has_one_correct_answer = True
                    break
        if not has_one_correct_answer:
            raise ValidationError('Mark at least one answer as correct.', code='no_correct_answer')


class TakeQuizForm(forms.ModelForm):
    answer = forms.ModelChoiceField(
        queryset=Answer.objects.none(),
        widget=forms.RadioSelect(),
        required=True,
        empty_label=None)

    class Meta:
        model = StudentAnswer
        fields = ('answer', )

    def __init__(self, *args, **kwargs):
        question = kwargs.pop('question')
        super().__init__(*args, **kwargs)
        self.fields['answer'].queryset = question.answers.order_by('text')
