from django.contrib.auth import logout
from rest_framework.exceptions import NotFound
from rest_framework.generics import RetrieveAPIView, RetrieveDestroyAPIView, ListCreateAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import IsAuthenticated
from classroom.models import Measurement
from classroom.serializers import UserSerializer, MeasurementSerializer


class UserAPIView(CreateModelMixin, RetrieveDestroyAPIView):
    """
    Login, view the current user or logout.
    """
    name = 'Authentication'
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user

    def post(self, *args, **kwargs):
        return self.create(*args, **kwargs)

    def perform_destroy(self, instance):
        logout(self.request)


class MeasurementAPIView(ListCreateAPIView):
    """
    Browse the user's station's measurements or save a new measurement.
    """
    name = 'Measurements'
    serializer_class = MeasurementSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        return Measurement.objects.filter(station__user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(station=self.request.user.station)


class LatestMeasurementAPIView(RetrieveAPIView):
    name = 'Latest measurement'
    serializer_class = MeasurementSerializer
    permission_classes = (IsAuthenticated, )

    def get_object(self):
        try:
            return Measurement.objects \
                .filter(station__user=self.request.user) \
                .latest()
        except Measurement.DoesNotExist:
            raise NotFound