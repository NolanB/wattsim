from django.contrib.auth import get_user_model, authenticate, login
from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed
from classroom.models import Measurement


class UserSerializer(serializers.ModelSerializer):
    station = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = get_user_model()
        fields = (
            'id',
            'username',
            'password',
            'auth_token',
            'station',
        )
        read_only_fields = (
            'id',
            'auth_token',
            'station',
        )
        extra_kwargs = {
            'username': {
                'validators': [],
            },
            'password': {
                'write_only': True,
                'style': {
                    'input_type': 'password',
                }
            },
        }

    def create(self, data):
        assert 'request' in self.context, 'Missing HTTP request in serializer context'
        user = authenticate(self.context['request'], **data)
        if not user:
            raise AuthenticationFailed()

        login(self.context['request'], user)
        return user


class MeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Measurement
        fields = (
            'id',
            'recorded',
            'solar',
            'wind',
            'hydro',
        )
        read_only_fields = (
            'id',
            'recorded',
        )