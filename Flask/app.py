from flask import Flask, render_template,request, redirect, url_for, make_response
import plotly
import plotly.graph_objs as go
import wattsim.WattsimDB as WBD
import pandas as pd
import numpy as np
import json
import datetime
from dotenv import load_dotenv

app = Flask(__name__)

@app.route("/", methods=["POST"])
def post_classroom():
    try:
        classe_name = request.form.get("classe_name")
        print("classe_name : ", classe_name)
        resp = make_response(redirect(url_for("index")))
        resp.set_cookie('classe_name', classe_name)
        resp_2 = make_response(redirect(url_for('create_plot')))
        resp_2.set_cookie('classe_name', classe_name)
        return resp
    except:
        redirect(url_for("index"))
        return render_template("index.html")

@app.route('/', methods=["GET"])
def index():
    classe_name = request.cookies.get("classe_name")
    conn = WBD.connexion_DB()
    list_users=[]
    try:
        if conn is not None:
            users = WBD.select_student_in_classroom(conn, classe_name)
            for user in users:
                list_users.append(user[1])

        moyenne_classe_solar = []
        moyenne_classe_wind = []
        moyenne_classe_hydro = []
        list_classe_moy_solar = []
        list_classe_moy_wind = []
        list_classe_moy_hydro = []
        if conn is not None:
            users = WBD.select_student_in_classroom(conn, classe_name)
            for user in users:
                user_id = user[0]
                user_name = user[1]
                list_station = WBD.select_station(conn, user_id)
                id_station = list_station[0][0]
                measurements = WBD.select_measurement(conn, id_station)
                moy_solar = 0
                moy_wind = 0
                moy_hydro = 0        
                solar = 0
                wind = 0
                hydro = 0
                nb_measurements = 0
                for measurement in measurements:             
                    solar += measurement[1]
                    wind += measurement[2]
                    hydro += measurement[3]
                    nb_measurements += 1
                moy_solar = (solar/nb_measurements)
                moy_wind = (wind/nb_measurements)
                moy_hydro = (hydro/nb_measurements)
                list_classe_moy_solar.append(moy_solar)
                list_classe_moy_wind.append(moy_wind)
                list_classe_moy_hydro.append(moy_hydro)

        moyenne_classe_solar = round(sum(list_classe_moy_solar)/len(list_classe_moy_solar),2) if len(list_classe_moy_solar) != 0 else 0
        moyenne_classe_wind = round(sum(list_classe_moy_wind)/len(list_classe_moy_wind),2) if len(list_classe_moy_wind) != 0 else 0
        moyenne_classe_hydro = round(sum(list_classe_moy_hydro)/len(list_classe_moy_hydro),2) if len(list_classe_moy_hydro) != 0 else 0
        feature = list_users[0]
    except:
        feature = ''
        moyenne_classe_solar = 0
        moyenne_classe_wind = 0
        moyenne_classe_hydro = 0
    bar = create_plot(feature)
    
    if conn is not None:
        WBD.deconnexion_DB(conn)
    return render_template('index.html', plot=bar, solar=moyenne_classe_solar, wind=moyenne_classe_wind, hydro=moyenne_classe_hydro, list_users= list_users)

@app.route('/', methods=["GET"])
def create_plot(feature):
    classe_name = request.cookies.get("classe_name")
    conn = WBD.connexion_DB()
    data =[]
    moyenne_classe_solar = []
    moyenne_classe_wind = []
    moyenne_classe_hydro = []
    list_classe_moy_solar = []
    list_classe_moy_wind = []
    list_classe_moy_hydro = []
    if conn is not None:
        list_users = []
        users = WBD.select_student_in_classroom(conn, classe_name)
        for user in users:
            list_users.append(user[1])
        if feature in list_users:
            for user in users:
                if feature == user[1]:
                    user_id = user[0]
                    user_name = user[1]
                    list_station = WBD.select_station(conn, user_id)
                    id_station = list_station[0][0]
                    measurements = WBD.select_measurement(conn, id_station)
                    moy_solar = 0
                    moy_wind = 0
                    moy_hydro = 0        
                    solar = 0
                    wind = 0
                    hydro = 0
                    nb_measurements = 0
                    for measurement in measurements:             
                        solar += measurement[1]
                        wind += measurement[2]
                        hydro += measurement[3]
                        nb_measurements += 1
                    moy_solar = (solar/nb_measurements)
                    moy_wind = (wind/nb_measurements)
                    moy_hydro = (hydro/nb_measurements)
                    list_classe_moy_solar.append(moy_solar)
                    list_classe_moy_wind.append(moy_wind)
                    list_classe_moy_hydro.append(moy_hydro)

            data = [
                go.Bar(name='solar', x=[user_name], y=[moy_solar]),
                go.Bar(name='wind', x=[user_name], y=[moy_wind]),
                go.Bar(name='hydro', x=[user_name], y=[moy_hydro])
            ]
        elif feature == 'classe':
            for user in users:
                user_id = user[0]
                user_name = user[1]
                list_station = WBD.select_station(conn, user_id)
                id_station = list_station[0][0]
                measurements = WBD.select_measurement(conn, id_station)
                moy_solar = 0
                moy_wind = 0
                moy_hydro = 0        
                solar = 0
                wind = 0
                hydro = 0
                nb_measurements = 0
                for measurement in measurements:             
                    solar += measurement[1]
                    wind += measurement[2]
                    hydro += measurement[3]
                    nb_measurements += 1
                moy_solar = (solar/nb_measurements)
                moy_wind = (wind/nb_measurements)
                moy_hydro = (hydro/nb_measurements)
                list_classe_moy_solar.append(moy_solar)
                list_classe_moy_wind.append(moy_wind)
                list_classe_moy_hydro.append(moy_hydro)
            data = [
                go.Bar(name='solar', x=list_users, y=list_classe_moy_solar),
                go.Bar(name='wind', x=list_users, y=list_classe_moy_wind),
                go.Bar(name='hydro', x=list_users, y=list_classe_moy_hydro)
            ]
            
    graphJSON = json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)
    moyenne_classe_solar = round(sum(list_classe_moy_solar)/len(list_classe_moy_solar),2) if len(list_classe_moy_solar) != 0 else 0
    moyenne_classe_wind = round(sum(list_classe_moy_wind)/len(list_classe_moy_wind),2) if len(list_classe_moy_wind) != 0 else 0
    moyenne_classe_hydro = round(sum(list_classe_moy_hydro)/len(list_classe_moy_hydro),2) if len(list_classe_moy_hydro) != 0 else 0
    
    if conn is not None:
        WBD.deconnexion_DB(conn) 
    return graphJSON

@app.route('/bar', methods=['GET', 'POST'])
def change_features():
    feature = request.args['selected']
    graphJSON = create_plot(feature)
    return graphJSON

if __name__ == '__main__':
    app.run()
