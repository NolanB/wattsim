# from requests_html import HTML, HTMLSession
from collections import defaultdict
import psycopg2
import os
from datetime import datetime
from werkzeug.security import check_password_hash, generate_password_hash
import pandas as pd
import getpass

# etablir une connection a une DataBase
def connexion_DB():
    """
        Etablit une connection a une DataBase Postgres 
        Parametres:
        fichierDB = le nom de la Base de Données
        user, pswd, host, port = parametres par defaut pour la connection à postgres en local
    """
    conn = ''
    try:
        conn = psycopg2.connect(user = os.environ.get('DB_USER'),
                                password = os.environ.get('DB_PASSWORD'),
                                host = os.environ.get('DB_HOST'),
                                port = os.environ.get('DB_PORT'),
                                database = os.environ.get('DB_NAME')
                            )
                                
        cur = conn.cursor()
        # Print PostgreSQL Connection properties
        # print ( "Proprietes de la connection Postgres: ",conn.get_dsn_parameters(),"\n")

        # Print PostgreSQL version
        # cur.execute("SELECT version();")
        #record = cur.fetchone()
        # print("Version de PostgreSQL : ", record,"\n")
        cur.close()
    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
    return conn

#fermer la connection a la database
def deconnexion_DB(conn):
    if(conn):
        conn.close()
        print("PostgreSQL connection is closed") 

def insert_create_schoolroom(conn, schoolroom_name):
    if conn is not None:
        cur = conn.cursor()
        try :
            sql_schoolroom = f"""
                INSERT INTO classroom_schoolroom (name) VALUES ('{schoolroom_name}') RETURNING id;
            """     
            cur.execute(sql_schoolroom)
            schoolroom_id = cur.fetchone()[0]
            conn.commit()
            return schoolroom_id
        except:
            Exception(f"La classe nommée {schoolroom_name} n'est pas créée")
            return None

def insert_create_teacher(conn, schoolroom_id, username, first_name, last_name, email, password):
    if (conn):
        sql_user = f""" 
            INSERT INTO classroom_user (
                username,
                first_name,
                last_name,
                email,
                password,
                date_joined,
                is_staff,
                is_active,
                is_superuser,
                is_student,
                is_teacher
            ) 
            VALUES (
                %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s
            ) RETURNING id;
        """
        #create user
        cur = conn.cursor()
        cur.execute(sql_user, (username, first_name, last_name, email, password, datetime.now(), False, True, False, False, True))
        user_id = cur.fetchone()[0]
        conn.commit()

        sql_teacher = f"""
            INSERT INTO classroom_teacher (
                user_id, 
                schoolroom_id
            ) 
            VALUES (
                %s, 
                %s
            );
        """
        print(sql_teacher)     
        cur.execute(sql_teacher, (user_id, schoolroom_id))
        conn.commit()
        return (user_id)

def insert_create_student(conn, schoolroom_id, username, first_name, last_name, email, password):
    if (conn):
        sql_user = f""" 
            INSERT INTO classroom_user (
                username,
                first_name,
                last_name,
                email,
                password,
                date_joined,
                is_staff,
                is_active,
                is_superuser,
                is_student,
                is_teacher
            ) 
            VALUES (
                %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s
            ) RETURNING id;
        """
        cur = conn.cursor()
        #create user
        cur.execute(sql_user, (username, first_name, last_name, email, password, datetime.now(), False, True, False, True, False))
        user_id = cur.fetchone()[0]
        conn.commit()

        sql_student = f"""
            INSERT INTO classroom_student (
                user_id, 
                schoolroom_id
            ) 
            VALUES (
                %s, 
                %s
            );
        """
        #create student  
        cur.execute(sql_student, (user_id, schoolroom_id))
        conn.commit()
        return (user_id)

def insert_station(conn, user_id):
    if (conn):
        sql_station = f"""
            INSERT INTO classroom_station (user_id) VALUES ('{user_id}') RETURNING id;
        """
    
        cur = conn.cursor()
        # print(sql_station, (user_id), " RETURNING id;")
        #create station
        cur.execute(sql_station)
        station_id = cur.fetchone()[0]
        conn.commit()
        return (station_id)

def insert_measurement(conn, station_id, recorded=datetime.now(), solar=0.0, wind=0.0, hydro=0.0):
    if (conn):
        sql_measurement = f"""
            INSERT INTO classroom_measurement (
                recorded,
                solar,
                wind,
                hydro,
                station_id
            ) 
            VALUES (
                %s,
                %s,
                %s,
                %s,
                %s
            ) RETURNING id;
        """
        cur = conn.cursor()
        #create measurement  
        cur.execute(sql_measurement, (recorded, solar, wind, hydro, station_id))
        measurement_id = cur.fetchone()[0]
        conn.commit()
        return (measurement_id)

def select_classroom_id(conn, schoolroom_name):
    if conn is not None:
        cur = conn.cursor() 
        select_sql = f'''
            select id from classroom_schoolroom where name = '{schoolroom_name}';
        '''        
        cur.execute(select_sql)
        return cur.fetchone()[0]

def select_all_student(conn):
    if conn is not None:
        cur = conn.cursor() 
        select_sql = f'''
            select id, username from classroom_user where classroom_user.id in (select user_id from classroom_student);
        '''        
        cur.execute(select_sql)
        return cur.fetchall()

def select_student_in_classroom(conn, schoolroom_name):
    if conn is not None:
        cur = conn.cursor() 
        select_sql = f'''
            select id, username from classroom_user where classroom_user.id in (select user_id from classroom_student
            where classroom_student.schoolroom_id
            in (select id from classroom_schoolroom where classroom_schoolroom.name = '{schoolroom_name}'));
        '''        
        cur.execute(select_sql)
        return cur.fetchall()

def select_student_in_classroom_and_teacher(conn, schoolroom_name):
    if conn is not None:
        cur = conn.cursor() 
        select_sql = f'''
            select id, username from classroom_user where classroom_user.id in (select user_id from classroom_student
            where classroom_student.schoolroom_id
            in (select id from classroom_schoolroom where classroom_schoolroom.name = '{schoolroom_name}')) or 
            classroom_user.id in (select user_id from classroom_teacher where classroom_teacher.schoolroom_id 
            in (select id from classroom_schoolroom where classroom_schoolroom.name = '{schoolroom_name}'));
        '''        
        cur.execute(select_sql)
        return cur.fetchall()

def select_station(conn, user_id):
    if conn is not None:
        cur = conn.cursor()  
        select_sql = f'''
            select * from classroom_station where user_id = '{user_id}';
        '''        
        cur.execute(select_sql)
        return cur.fetchall()

def select_measurement(conn, station_id):
    if conn is not None:
        cur = conn.cursor()
        select_sql = f'''
            select recorded, solar, wind, hydro from classroom_measurement where station_id = '{station_id}'; 
        '''        
        cur.execute(select_sql)
        return cur.fetchall()

def select_measurement_in_classroom(conn, schoolroom_name):
    list_users = select_student_in_classroom(conn, schoolroom_name)
    list_measurement = []
    try:
        for user in list_users:
            user_id = user[0]
            user_name = user[1]
            list_station = select_station(conn, user_id)
            id_station = list_station[0][0]
            measurements = select_measurement(conn, id_station)
            for measurement in measurements:             
                recorded = measurement[0]
                solar = measurement[1]
                wind = measurement[2]
                hydro = measurement[3]
                list_measurement.append([user_id, user_name, id_station, recorded, solar, wind, hydro])
        return list_measurement
    except:
        return []

def select_measurement_in_classroom_with_teacher(conn, schoolroom_name):
    list_users = select_student_in_classroom_and_teacher(conn, schoolroom_name)
    list_measurement = []
    try:
        for user in list_users:
            user_id = user[0]
            user_name = user[1]
            list_station = select_station(conn, user_id)
            id_station = list_station[0][0]
            measurements = select_measurement(conn, id_station)
            for measurement in measurements:             
                recorded = measurement[0]
                solar = measurement[1]
                wind = measurement[2]
                hydro = measurement[3]
                list_measurement.append([user_id, user_name, id_station, recorded, solar, wind, hydro])
        return list_measurement
    except:
        return []

def input_create_user():
    username = str(input("username : "))
    first_name = str(input("first_name : "))
    last_name = str(input("last_name : "))
    email = str(input("email : "))
    pswd = getpass.getpass('password:')
    hash = 'pbkdf2:sha256'
    password_hash = generate_password_hash(pswd, hash)
    return username, first_name, last_name, email, password_hash

def print_measurements(measurements:list):
    user_id = ''
    user_name = ''
    id_station= ''

    for measurement in measurements:
        date = measurement[3]
        solar = measurement[4]
        wind = measurement[5]
        hydro = measurement[6]
        if user_name != measurement[1]:
            user_id = measurement[0]
            user_name = measurement[1]
            id_station= measurement[2]
            print("___________________________________________________________________________")
            print("Name :          ", user_name)
            print("Id_User :       ", user_id)
            print("Id_Station :    ", id_station)
        print("    -------------------------------------------------------------------    ")             
        print("     recorded : ", date)
        print("     solar    : ", solar)
        print("     wind     : ", wind)
        print("     hydro    : ", hydro)
        print("    -------------------------------------------------------------------    ")

if __name__ == "__main__":
    conn = connexion_DB()
    schoolroom_name = '3e A'
    # schoolroom_id = insert_create_schoolroom(conn, schoolroom_name)

    classroom_id = select_classroom_id(conn, schoolroom_name)

    # username, first_name, last_name, email, password_hash = input_create_user()
    # teacher_id = insert_create_teacher(conn, classroom_id, username, first_name, last_name, email, password_hash)
    # station_teacher_id = insert_station(conn, teacher_id)

    username, first_name, last_name, email, password_hash = input_create_user()
    student_id = insert_create_student(conn, classroom_id, username, first_name, last_name, email, password_hash)
    station_student_id = insert_station(conn, student_id)

    # measurement_id = insert_measurement(conn, station_teacher_id, datetime.now(), 24.5, 25.6, 32.5)
    # measurement_id = insert_measurement(conn, station_teacher_id, datetime.now(), 8.3, 90.4, 10.5)
    # measurement_id = insert_measurement(conn, station_teacher_id, datetime.now(), 19.5, 2.6, 24.5)
    # measurement_id = insert_measurement(conn, station_teacher_id, datetime.now(), 43.5, 53.6, 12.2)

    measurement_id = insert_measurement(conn, station_student_id, datetime.now(), 90.5, 54.6, 32.5)
    measurement_id = insert_measurement(conn, station_student_id, datetime.now(), 12.3, 89.4, 4.5)
    measurement_id = insert_measurement(conn, station_student_id, datetime.now(), 45.5, 14.6, 24.5)
    measurement_id = insert_measurement(conn, station_student_id, datetime.now(), 73.5, 5.6, 41.2)

    list_users=[]
    # users = select_student_in_classroom(conn, schoolroom_name)
    users = select_student_in_classroom_and_teacher(conn, schoolroom_name)
    
    # for user in users:
    #     list_users.append(user[1])
    
    # print("List_user : ", list_users)

    # measurements = select_measurement_in_classroom(conn, schoolroom_name)
    measurements = select_measurement_in_classroom_with_teacher(conn, schoolroom_name)
    print_measurements(measurements)

    deconnexion_DB(conn)