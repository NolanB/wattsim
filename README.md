# Wattsim

## Lancer le projet en local

Pour commencer, il faut clone le projet sur votre machine local:

```bash
git clone https://gitlab.com/NolanB/wattsim.git or git@gitlab.com:NolanB/wattsim.git
```

Installer les requirements:

```bash
pipenv install -r requirements.txt
```

Lancer docker:
```bash
sudo docker-compose up -d
```
Créer une base:

```bash
cd Django/
python manage.py migrate
python manage.py createsuperuser
```

Lancer le development server:

```bash
python manage.py runserver
```

(il faut se deconnecter si on est log en mode superuser)
Le projet est disponible à **127.0.0.1:8000/**.

## Pour insérer des données en base
Pour insérer des données en base j'ai créer un fichier nommée WattsimDB.py (disponible à la racine) avec des fonctions d'insertions et d'interrogations. Il est aussi possible de renseigner les données via l'espace d'administration de Django à **127.0.0.1:8000/admin/**.

```bash
cd .. #Se placer à la racine du projet si ce n'est pas le cas
python WattsimDB.py #Possibiliter de modifier les valeurs et appels de fonctions dans le 'if __name__ == "__main__":' avant execution
```


## Petit exemple des données que pourrait afficher un professeur

Il faut entrer le nom d'une classe, et ensuite le professeur à accès aux données de cette classe.
(Il faut avoir créé des données en base pour les affichers et lancer le docker)

```bash
cd .. #Se placer à la racine du projet si ce n'est pas le cas(.env)
flask run
```
Le projet flask est disponible à **127.0.0.1:5000/**

## Running the Project Locally

Je n'ai pas réussi à implémenter les vues des professeurs et des élèves ainsi que de récupérer les données de l'api.
En revanche, il y a bien un système de login mis en place et la structure de la base de donnée permet d'ajouter des professeurs, des élèves ainsi que les données de leurs stations. Elle permettra aussi dans un futur, de créer des questionnaires via les proffesseurs et de pouvoir y répondre du côté des élèves.

## Pour aller plus loin

Dans **'/wattsim/Django/classroom/forms.py'** il faudrait pouvoir instancier les classes des élèves et proffesseurs. Les droits d'accès son géré dans **'/wattsim/Django/classroom/decorators.py'**. Ensuite il faudrait séparer les vues de facon à ce qu'un élève ne puisse pas créer de compte professeur (exemple: professeur qui se connecte pour pouvoir permettre aux élèves de créer un compte), et que les rendus soient différents dans chaque vues.

## Après modification :
**Il est possible de se balader dans les vues teachers et students et d'y créer des questionnaires. Il faut avant tout créer des classes et des thématiques (subjects) dans la partie administration de django, pour que les créations dans les vues fonctionnent. Les connexions dans les vues ne fonctionne qu'a partir des créations Students et Teachers dans les vues.**